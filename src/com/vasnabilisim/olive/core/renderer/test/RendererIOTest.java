package com.vasnabilisim.olive.core.renderer.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.renderer.RendererIO;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

public class RendererIOTest {

	@Test
	public void test() {
		try {
			ReportRenderer reportRenderer = RendererIO.read(RendererIOTest.class.getResourceAsStream("Renderer01.xml"));
			Paper paper = reportRenderer.getPaper();
			assertEquals("A4", paper.getName());
		} catch (BaseException e) {
			fail(e.getMessage());
		}
	}

}

package com.vasnabilisim.olive.core.renderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Menderes Fatih GUVEN
 */
public class PageRenderer extends Renderer {
	private static final long serialVersionUID = 7061605912464442795L;

	protected ReportRenderer report;
	
	protected List<SimpleRenderer> children;
	
	
	public PageRenderer() {
		super();
		children = new ArrayList<>();
	}

	public PageRenderer(PageRenderer source) {
		super(source);
		children = new ArrayList<>(source.children.size());
		for(SimpleRenderer renderer : source.children)
			this.addChild(renderer.clone());
	}

	@Override
	public PageRenderer clone() {
		return new PageRenderer(this);
	}

	@Override
	public String getId() {
		return "Page";
	}
	
	public List<SimpleRenderer> getChildren() {
		return Collections.unmodifiableList(children);
	}

	public void addChild(SimpleRenderer renderer) {
		children.add(renderer);
		renderer.setParent(this);
	}

	public void setReport(ReportRenderer report) {
		this.report = report;
	}
}

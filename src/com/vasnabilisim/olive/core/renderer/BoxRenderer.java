package com.vasnabilisim.olive.core.renderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class BoxRenderer extends RectangularRenderer {
	private static final long serialVersionUID = -5721272376036286485L;

	public BoxRenderer() {
		super();
	}
	
	public BoxRenderer(BoxRenderer source) {
		super(source);
	}
	
	@Override
	public String getId() {
		return "Box";
	}

	@Override
	public BoxRenderer clone() {
		return new BoxRenderer(this);
	}

}

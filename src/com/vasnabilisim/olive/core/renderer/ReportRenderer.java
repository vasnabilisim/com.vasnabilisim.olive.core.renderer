package com.vasnabilisim.olive.core.renderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.property.PaperProperty;
import com.vasnabilisim.olive.core.property.PropertyFactoryX;

/**
 * @author Menderes Fatih GUVEN
 */
public class ReportRenderer extends Renderer {
	private static final long serialVersionUID = 3501182820691219169L;

	protected PaperProperty paperProperty;
	protected List<PageRenderer> pages;
	
	public ReportRenderer() {
		this.paperProperty = PropertyFactoryX.createPaperProperty("Paper", "Paper", "Paper", "Paper for overall design", Paper.Default.clone());
		this.properties.add(this.paperProperty);
		this.pages = new ArrayList<>();
	}

	public ReportRenderer(ReportRenderer source) {
		super(source);
		this.paperProperty = source.paperProperty.clone();
		this.properties.add(this.paperProperty);
		this.pages = new ArrayList<>(source.pages.size());
		for(PageRenderer renderer : source.pages)
			this.addPage(renderer.clone());
	}

	@Override
	public ReportRenderer clone() {
		return new ReportRenderer(this);
	}

	@Override
	public String getId() {
		return "Report";
	}
	
	public List<PageRenderer> getPages() {
		return Collections.unmodifiableList(pages);
	}

	public void addPage(PageRenderer renderer) {
		pages.add(renderer);
		renderer.setReport(this);
	}

	public Paper getPaper() {
		return paperProperty.getValue();
	}
	
	public void setPaper(Paper value) {
		paperProperty.setValue(value);
	}

}

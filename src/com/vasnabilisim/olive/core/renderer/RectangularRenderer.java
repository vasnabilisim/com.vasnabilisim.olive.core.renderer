package com.vasnabilisim.olive.core.renderer;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Point;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.paint.Border;
import com.vasnabilisim.olive.core.property.BorderProperty;
import com.vasnabilisim.olive.core.property.PropertyFactory;
import com.vasnabilisim.olive.core.property.RectangleProperty;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class RectangularRenderer extends AreaRenderer {
	private static final long serialVersionUID = -9077150807513509648L;

	protected RectangleProperty boundsProperty;
	protected BorderProperty borderProperty;
	
	public RectangularRenderer() {
		this.boundsProperty = PropertyFactory.createRectangleProperty("Content", "Bounds", "Bounds", "Bounds x, y, width, height", Rectangle.Zero.clone());
		this.borderProperty = PropertyFactory.createBorderProperty("Content", "Border", "Border", "Border to draw around element", Border.Empty.clone());
		this.properties.add(this.boundsProperty);
		this.properties.add(this.borderProperty);
	}
	
	public RectangularRenderer(RectangularRenderer source) {
		super(source);
		this.boundsProperty = source.boundsProperty.clone();
		this.borderProperty = source.borderProperty.clone();
		this.properties.add(this.boundsProperty);
		this.properties.add(this.borderProperty);
	}
	
	public Rectangle getBounds() {
		return boundsProperty.getValue();
	}

	public void setBounds(Rectangle value) {
		boundsProperty.setValue(value);
	}

	public Point getLocation() {
		return boundsProperty.getValue().getPoint();
	}
	
	public void setPoint(Point value) {
		boundsProperty.getValue().setPoint(value);
	}
	
	public Dimension getSize() {
		return boundsProperty.getValue().getDimension();
	}
	
	public void setSize(Dimension value) {
		boundsProperty.getValue().setDimension(value);
	}
	
	public float getX() {
		return boundsProperty.getValue().getX();
	}
	
	public void setX(float x) {
		boundsProperty.getValue().setX(x);
	}
	
	public float getY() {
		return boundsProperty.getValue().getY();
	}
	
	public void setY(float y) {
		boundsProperty.getValue().setY(y);
	}
	
	public float getWidth() {
		return boundsProperty.getValue().getWidth();
	}
	
	public void setWidth(float width) {
		boundsProperty.getValue().setWidth(width);
	}
	
	public float getHeight() {
		return boundsProperty.getValue().getHeight();
	}
	
	public void setHeight(float height) {
		boundsProperty.getValue().setHeight(height);
	}
	
	public Border getBorder() {
		return borderProperty.getValue();
	}
	
	public void setBorder(Border value) {
		borderProperty.setValue(value);
	}
}

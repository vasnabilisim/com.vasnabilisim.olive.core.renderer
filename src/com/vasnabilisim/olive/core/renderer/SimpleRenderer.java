package com.vasnabilisim.olive.core.renderer;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class SimpleRenderer extends Renderer {
	private static final long serialVersionUID = 8214499571182239181L;

	public SimpleRenderer() {
	}
	
	public SimpleRenderer(SimpleRenderer source) {
		super(source);
	}
	
	@Override
	public abstract SimpleRenderer clone();
}

package com.vasnabilisim.olive.core.renderer;

import com.vasnabilisim.olive.core.geom.Point;
import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.property.ColorProperty;
import com.vasnabilisim.olive.core.property.FloatProperty;
import com.vasnabilisim.olive.core.property.PointProperty;
import com.vasnabilisim.olive.core.property.PropertyFactory;

/**
 * @author Menderes Fatih GUVEN
 */
public class LineRenderer extends SimpleRenderer {
	private static final long serialVersionUID = 8795861992079318605L;

	protected PointProperty fromPointProperty;
	protected PointProperty toPointProperty;
	protected ColorProperty colorProperty;
	protected FloatProperty thicknessProperty;
	
	public LineRenderer() {
		this.fromPointProperty = PropertyFactory.createPointProperty("Line", "Begin Point", "FromPoint", "Begining coordinates of the line", Point.Zero.clone());
		this.toPointProperty = PropertyFactory.createPointProperty("Line", "End Point", "ToPoint", "Ending coordinates of the line", Point.Zero.clone());
		this.colorProperty = PropertyFactory.createColorProperty("Line", "Color", "Color", "Line color", Color.Black);
		this.thicknessProperty = PropertyFactory.createFloatProperty("Line", "Thickness", "Thickness", "Thickness of the line", 1f);
		this.properties.add(this.fromPointProperty);
		this.properties.add(this.toPointProperty);
		this.properties.add(this.colorProperty);
		this.properties.add(this.thicknessProperty);
	}
	
	public LineRenderer(LineRenderer source) {
		super(source);
		this.fromPointProperty = source.fromPointProperty.clone();
		this.toPointProperty = source.toPointProperty.clone();
		this.colorProperty = source.colorProperty.clone();
		this.thicknessProperty = source.thicknessProperty.clone();
		this.properties.add(this.fromPointProperty);
		this.properties.add(this.toPointProperty);
		this.properties.add(this.colorProperty);
		this.properties.add(this.thicknessProperty);
	}
	
	@Override
	public String getId() {
		return "Line";
	}

	@Override
	public LineRenderer clone() {
		return new LineRenderer(this);
	}
	
	public Point getFromPoint() {
		return fromPointProperty.getValue();
	}
	
	public void setFromPoint(Point value) {
		fromPointProperty.setValue(value);
	}
	
	public Point getToPoint() {
		return toPointProperty.getValue();
	}
	
	public void setToPoint(Point value) {
		toPointProperty.setValue(value);
	}
	
	public Color getColor() {
		return colorProperty.getValue();
	}
	
	public void setColor(Color value) {
		colorProperty.setValue(value);
	}
	
	public Float getThickness() {
		return thicknessProperty.getValue();
	}
	
	public void setThickness(Float value) {
		thicknessProperty.setValue(value);
	}
}

package com.vasnabilisim.olive.core.renderer.export;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.RectangularRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;
import com.vasnabilisim.olive.core.renderer.export.util.GBorder;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class GRectangularRenderer<R extends RectangularRenderer> extends GAreaRenderer<R> {

	@Override
	protected void renderArea(GraphicsExporter exporter, ReportRenderer reportRenderer, PageRenderer pageRenderer, R r,
			Graphics2D g) throws BaseException 
	{
		Shape gOldClip = g.getClip();
		Rectangle2D.Float gBounds = toRectangle(exporter, reportRenderer.getPaper(), r.getBounds());
		g.setClip(gBounds);
		
		g.setColor(toColor(r.getBackground()));
		Rectangle2D.Float gRect = toRectangleAtOrigin(exporter, reportRenderer.getPaper(), r.getBounds());
		g.fill(gRect);
		
		renderRectangular(exporter, reportRenderer, pageRenderer, r, g);
		
		GBorder gBorder = toBorder(exporter, reportRenderer.getPaper(), r.getBorder());
		gBorder.paintBorder(g, gRect);
		
		g.setClip(gOldClip);
	}

	protected abstract void renderRectangular(GraphicsExporter exporter, ReportRenderer reportRenderer, PageRenderer pageRenderer, R r,
			Graphics2D g) throws BaseException;

	protected java.awt.geom.Rectangle2D.Float toRectangle(GraphicsExporter exporter, Paper paper, com.vasnabilisim.olive.core.geom.Rectangle rect) {
		return new java.awt.geom.Rectangle2D.Float(
				toPixels(exporter, paper, rect.x), 
				toPixels(exporter, paper, rect.y), 
				toPixels(exporter, paper, rect.w), 
				toPixels(exporter, paper, rect.h) 
		);
	}

	protected java.awt.geom.Rectangle2D.Float toRectangleAtOrigin(GraphicsExporter exporter, Paper paper, com.vasnabilisim.olive.core.geom.Rectangle rect) {
		return new java.awt.geom.Rectangle2D.Float(
				0f, 
				0f, 
				toPixels(exporter, paper, rect.w), 
				toPixels(exporter, paper, rect.h) 
		);
	}

	/**
	 * Converts given border to swing border.
	 * @param border
	 * @return
	 */
	protected GBorder toBorder(GraphicsExporter exporter, Paper paper, com.vasnabilisim.olive.core.paint.Border border) {
		GBorder gBorder = new GBorder();
		gBorder.setLeftColor(toColor(border.lColor));
		gBorder.setLeftStroke(new BasicStroke(toPixels(exporter, paper, border.thickness.l)));
		gBorder.setTopColor(toColor(border.tColor));
		gBorder.setTopStroke(new BasicStroke(toPixels(exporter, paper, border.thickness.t)));
		gBorder.setRightColor(toColor(border.rColor));
		gBorder.setRightStroke(new BasicStroke(toPixels(exporter, paper, border.thickness.r)));
		gBorder.setBottomColor(toColor(border.bColor));
		gBorder.setBottomStroke(new BasicStroke(toPixels(exporter, paper, border.thickness.b)));
		return gBorder;
	}
}

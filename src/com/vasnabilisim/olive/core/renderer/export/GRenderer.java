package com.vasnabilisim.olive.core.renderer.export;

import java.awt.Graphics2D;
import java.io.IOException;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.paper.PaperUnit;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;
import com.vasnabilisim.olive.core.renderer.SimpleRenderer;
import com.vasnabilisim.util.ClassComparator;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class GRenderer<R extends SimpleRenderer> {

	private static final TreeMap<Class<?>, GRenderer<?>> rendererMap = new TreeMap<>(new ClassComparator());
	
	public static <R extends SimpleRenderer> void register(GRenderer<R> pdfRenderer) {
		rendererMap.put(pdfRenderer.rendererClass(), pdfRenderer);
	}
	
	@SuppressWarnings("unchecked")
	public static <R extends SimpleRenderer> GRenderer<R> get(Class<R> rendererClass) {
		return (GRenderer<R>) rendererMap.get(rendererClass);
	}
	
	@SuppressWarnings("unchecked")
	public static <R extends SimpleRenderer> GRenderer<R> get(R renderer) {
		return (GRenderer<R>) get(renderer.getClass());
	}
	
	static {
		register(new GLineRenderer());
		register(new GTextRenderer());
	}
	
	protected GRenderer() {
	}
	
	/**
	 * Returns the class pdf-rendered by this.
	 * @return
	 */
	public abstract Class<R> rendererClass();

	/**
	 * Pdf-renders the given renderer to given graphics content.
	 * @param exporter
	 * @param reportRenderer
	 * @param pageRenderer
	 * @param r
	 * @param g
	 * @throws IOException
	 * @throws BaseException
	 */
	public abstract void render(
			GraphicsExporter exporter, 
			ReportRenderer reportRenderer, 
			PageRenderer pageRenderer, 
			R r, 
			Graphics2D g) throws BaseException;
	
	/**
	 * Converts given color to awt color.
	 * @param color
	 * @return
	 */
	public static java.awt.Color toColor(com.vasnabilisim.olive.core.paint.Color color) {
		return new java.awt.Color(color.r, color.g, color.b, color.o);
	}
	
	/**
	 * Convert given value to pixels. 
	 * Graphics exporter may use different resolutions. 
	 * In order to get right pixel value calculation is done at exporter.
	 * @param exporter
	 * @param unit
	 * @param value
	 * @return
	 */
	protected float toPixels(GraphicsExporter exporter, PaperUnit unit, float value) {
		return exporter.toPixels(unit, value);
	}

	/**
	 * @see GRenderer#toPixels(GraphicsExporter, PaperUnit, float)
	 */
	protected float toPixels(GraphicsExporter exporter, Paper paper, float value) {
		return exporter.toPixels(paper.getUnit(), value);
	}
}

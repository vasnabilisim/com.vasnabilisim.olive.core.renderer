package com.vasnabilisim.olive.core.renderer.export;

import java.io.OutputStream;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

/**
 * Exporter interface. 
 * Used to export report renderer to different formats.
 * 
 * @author Menderes Fatih GUVEN
 */
public interface Exporter {

	/**
	 * Name of the exporter.
	 * @return
	 */
	String name();

	/**
	 * Exports given reportRenderer to given output stream.
	 * @param reportRenderer
	 * @param out
	 * @throws BaseException
	 */
	void export(ReportRenderer reportRenderer, OutputStream out) throws BaseException;
}

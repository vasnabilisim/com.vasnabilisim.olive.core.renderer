package com.vasnabilisim.olive.core.renderer.export;

import java.awt.Graphics2D;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.BoxRenderer;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class GBoxRenderer extends GRectangularRenderer<BoxRenderer> {

	@Override
	public Class<BoxRenderer> rendererClass() {
		return BoxRenderer.class;
	}

	@Override
	protected void renderRectangular(GraphicsExporter exporter, ReportRenderer reportRenderer,
			PageRenderer pageRenderer, BoxRenderer r, Graphics2D g) throws BaseException {
	}
}

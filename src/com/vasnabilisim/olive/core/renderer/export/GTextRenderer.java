package com.vasnabilisim.olive.core.renderer.export;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.geom.HorizontalAlignment;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.geom.VerticalAlignment;
import com.vasnabilisim.olive.core.paint.FontStyle;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;
import com.vasnabilisim.olive.core.renderer.TextRenderer;
import com.vasnabilisim.util.Pair;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public class GTextRenderer extends GRectangularRenderer<TextRenderer> {

	@Override
	public Class<TextRenderer> rendererClass() {
		return TextRenderer.class;
	}
	
	@Override
	protected void renderRectangular(GraphicsExporter exporter, ReportRenderer reportRenderer,
			PageRenderer pageRenderer, TextRenderer r, Graphics2D g) throws BaseException 
	{
		String text = r.getText();
		if(StringUtil.isEmpty(text))
			return;
		
		java.awt.geom.Rectangle2D.Float imageableRectangle = toImageableRectangle(exporter, reportRenderer.getPaper(), r);
		
		java.awt.Font awtFont = toFont(r.getFont());
		FontMetrics fontMetrics = g.getFontMetrics(awtFont);
		float fontLeading = fontMetrics.getLeading();
		float fontAscent = fontMetrics.getAscent();
		float fontDescent = fontMetrics.getAscent();
		List<Pair<String, Float>> lines = toLines(fontMetrics, text, imageableRectangle.width, r.getMaximumLines());

		g.setFont(awtFont);

		if(r.getAlignment().getVerticalAlignment() == VerticalAlignment.Center)
			renderFromCenter(
					g, 
					imageableRectangle, 
					fontLeading, fontAscent, fontDescent, 
					lines, 
					r.getAlignment().getHorizontalAlignment());
		else if(r.getAlignment().getVerticalAlignment() == VerticalAlignment.Top)
			renderFromTop(
					g, 
					imageableRectangle, 
					fontLeading, fontAscent, fontDescent, 
					lines, 
					r.getAlignment().getHorizontalAlignment());
		else if(r.getAlignment().getVerticalAlignment() == VerticalAlignment.Bottom)
			renderFromBottom(
					g, 
					imageableRectangle, 
					fontLeading, fontAscent, fontDescent, 
					lines, 
					r.getAlignment().getHorizontalAlignment());
	}
	
	/**
	 * Renders lines from top to bottom. 
	 * @param g
	 * @param rect
	 * @param fontLeading
	 * @param fontAscent
	 * @param fontDescent
	 * @param lines
	 * @param hAlignment
	 */
	protected void renderFromTop(
			Graphics2D g, 
			java.awt.geom.Rectangle2D.Float rect, 
			float fontLeading, float fontAscent,  float fontDescent, 
			List<Pair<String, Float>> lines, 
			HorizontalAlignment hAlignment) 
	{
		float x;
		float y = rect.y;
		float yMax = rect.y + rect.height + fontAscent;
		for(Pair<String, Float> line : lines) {
			if(y > yMax)
				return;
			x = calcX(hAlignment, rect, line.getSecond());
			y += fontAscent;
			g.drawString(line.getFirst(), x, y);
			y += fontDescent;
			y += fontLeading;
		}
	}
	
	/**
	 * Renders lines from bottom to top.
	 * @param g
	 * @param rect
	 * @param fontLeading
	 * @param fontAscent
	 * @param fontDescent
	 * @param lines
	 * @param hAlignment
	 */
	protected void renderFromBottom(
			Graphics2D g, 
			java.awt.geom.Rectangle2D.Float rect, 
			float fontLeading, float fontAscent,  float fontDescent, 
			List<Pair<String, Float>> lines, 
			HorizontalAlignment hAlignment) 
	{
		float x;
		float y = rect.y + rect.height;
		float yMin = rect.y - fontDescent;
		for(Pair<String, Float> line : lines) {
			if(y < yMin)
				return;
			x = calcX(hAlignment, rect, line.getSecond());
			y -= fontDescent;
			g.drawString(line.getFirst(), x, y);
			y -= fontAscent;
			y -= fontLeading;
		}
	}
	
	/**
	 * Render lines vertically centered.
	 * @param g
	 * @param rect
	 * @param fontLeading
	 * @param fontAscent
	 * @param fontDescent
	 * @param lines
	 * @param hAlignment
	 */
	protected void renderFromCenter(
			Graphics2D g, 
			java.awt.geom.Rectangle2D.Float rect, 
			float fontLeading, float fontAscent,  float fontDescent, 
			List<Pair<String, Float>> lines, 
			HorizontalAlignment hAlignment) 
	{
		float fontHeight = fontLeading + fontAscent + fontDescent;
		float textHeight = lines.size() * fontHeight;
		float x;
		float y = rect.y + (rect.height - textHeight) / 2; //TODO test if it is centered.
		float yMin = rect.y - fontDescent;
		float yMax = rect.y + rect.height + fontAscent;
		for(Pair<String, Float> line : lines) {
			if(y < yMin) {
				y += fontHeight;
				continue;
			}
			if(y > yMax)
				return;
			x = calcX(hAlignment, rect, line.getSecond());
			y += fontAscent;
			g.drawString(line.getFirst(), x, y);
			y += fontDescent;
			y += fontLeading;
		}
	}

	/**
	 * Calculates starting x coordinate. 
	 * @param hAlignment
	 * @param rect
	 * @param lineWidth
	 * @return
	 */
	protected float calcX(HorizontalAlignment hAlignment, java.awt.geom.Rectangle2D.Float rect, float lineWidth) {
		switch(hAlignment) {
		case Center		: return rect.x + (rect.width - lineWidth) / 2f;
		case Right		:
		case Trailing	: return rect.x + rect.width - lineWidth;
		case Left		:
		case Leading	:
		default			: return rect.x;
		}
	}
	
	/**
	 * Splits given text from line breaks. 
	 * Also word wraps splitted texts according to given maxTextWidth. 
	 * Text lengths calculated with given fontMetrics.
	 * @param fontMetrics
	 * @param text
	 * @param maxTextWidth
	 * @param maximumLines
	 * @return
	 */
	protected List<Pair<String, Float>> toLines(FontMetrics fontMetrics, String text, float maxTextWidth, int maximumLines) {
		if(StringUtil.isEmpty(text))
			Collections.emptyList();
		ArrayList<Pair<String, Float>> lineList = new ArrayList<>(maximumLines);
		String[] lines = text.split("\\r?\\n");
		for(String line : lines) {
			StringBuilder builder = new StringBuilder(line.length());
			StringTokenizer tokenizer = new StringTokenizer(line, " \t", true);
			String token;
			float lineWidth = 0f;
			float tokenWidth = 0f;
			while(tokenizer.hasMoreTokens()) {
				token = tokenizer.nextToken();
				if(builder.length() == 0 && (" ".equals(token) || "\t".equals(token)) )
					continue;
				tokenWidth = fontMetrics.stringWidth(token);
				if(lineWidth + tokenWidth > maxTextWidth) {
					lineList.add(new Pair<>(builder.toString(), lineWidth));
					if(lineList.size() == maximumLines)
						return lineList;
					lineWidth = 0f;
					builder = new StringBuilder(line.length());
				}
				builder.append(token);
				lineWidth += tokenWidth;
			}//while
			if(builder.length() >= 0)
				lineList.add(new Pair<>(builder.toString(), lineWidth));
		}//for
		return lineList;
	}

	/**
	 * Calculates and returns given text renderer's imageable rectangle. 
	 * Returns awt rectangle. 
	 * @param exporter
	 * @param paper
	 * @param r
	 * @return
	 */
	protected java.awt.geom.Rectangle2D.Float toImageableRectangle(GraphicsExporter exporter, Paper paper, TextRenderer r) {
		Rectangle bounds = r.getBounds();
		Insets insets = r.getBorder().thickness.merge(r.getPadding());
		return new java.awt.geom.Rectangle2D.Float(
				toPixels(exporter, paper, bounds.x + insets.l), 
				toPixels(exporter, paper, bounds.y + insets.t), 
				toPixels(exporter, paper, bounds.w - insets.l - insets.r), 
				toPixels(exporter, paper, bounds.h - insets.t - insets.b) 
		);
	}
	
	/**
	 * Converts given font to awt font. 
	 * @param font
	 * @return
	 */
	protected java.awt.Font toFont(com.vasnabilisim.olive.core.paint.Font font) {
		int awtStyle = 0;
		Map<TextAttribute, Object> fontAttributes = new TreeMap<TextAttribute, Object>();
		for(FontStyle style : font.getStyles()) {
			switch (style) {
			case Bold		: awtStyle |= java.awt.Font.BOLD; break;
			case Italic		: awtStyle |= java.awt.Font.ITALIC; break;
			case Underline	: fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON); break;
			case Overline	: fontAttributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON); break;
			default		: break;
			}
		}
		java.awt.Font awtFont = new java.awt.Font(font.getName(), awtStyle, Math.round(font.getSize()));
		if(!fontAttributes.isEmpty())
			awtFont = awtFont.deriveFont(fontAttributes);
		return awtFont;
	}
}

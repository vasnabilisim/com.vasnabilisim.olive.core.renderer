package com.vasnabilisim.olive.core.renderer.export.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import javax.swing.border.Border;

/**
 * @author Menderes Fatih GUVEN
 */
public class GBorder implements Border, Cloneable, Serializable {
	private static final long serialVersionUID = -6755147817404096247L;

	private BasicStroke topStroke = null;
	private Color topColor = null;
	private BasicStroke rightStroke = null;
	private Color rightColor = null;
	private BasicStroke bottomStroke = null;
	private Color bottomColor = null;
	private BasicStroke leftStroke = null;
	private Color leftColor = null;

	/**
	 * Default constructor;
	 */
	public GBorder() {
		topStroke = new BasicStroke(1f);
		topColor = Color.BLACK;
		rightStroke = new BasicStroke(1f);
		rightColor = Color.BLACK;
		bottomStroke = new BasicStroke(1f);
		bottomColor = Color.BLACK;
		leftStroke = new BasicStroke(1f);
		leftColor = Color.BLACK;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public GBorder(GBorder source) {
		this.topStroke = source.topStroke;
		this.topColor = source.topColor;
		this.rightStroke = source.rightStroke;
		this.rightColor = source.rightColor;
		this.bottomStroke = source.bottomStroke;
		this.bottomColor = source.bottomColor;
		this.leftStroke = source.leftStroke;
		this.leftColor = source.leftColor;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public GBorder clone() {
		return new GBorder(this);
	}

	/**
	 * @see javax.swing.border.Border#paintBorder(java.awt.Component, java.awt.Graphics, int, int, int, int)
	 */
	public void paintBorder(Component c, Graphics _g, int x, int y, int width, int height) {
		Graphics2D g = (Graphics2D) _g;
		paintBorder(g, x, y, width, height, false);
	}

	/**
	 * Paints the border to given graphics instance.
	 * 
	 * @param g
	 * @param bounds
	 */
	public void paintBorder(Graphics2D g, Rectangle2D bounds) {
		Shape oldClip = g.getClip();
		Area area = new Area(oldClip);
		Rectangle2D newBounds = new Rectangle2D.Double(bounds.getX() - 1, bounds.getY() - 1, bounds.getWidth(), bounds.getHeight());
		area.intersect(new Area(bounds));
		g.setClip(area);
		paintBorder(g, newBounds.getX(), newBounds.getY(), newBounds.getWidth(), newBounds.getHeight(), true);
		g.setClip(oldClip);
	}

	/**
	 * Paints the border to given graphics instance.
	 * 
	 * @param g
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void paintBorder(Graphics2D g, double x, double y, double width, double height, boolean ceil) {
		java.awt.Stroke oldStroke = g.getStroke();
		Color oldColor = g.getColor();

		double X = 0;
		double Y = 0;
		Line2D line = null;
		if (isTopValid()) {
			g.setStroke(getTopStroke());
			g.setColor(getTopColor());

			Y = y + getTopStroke().getLineWidth() / 2.0;
			if (ceil)
				line = new Line2D.Double(Math.floor(x), Math.ceil(Y), Math.ceil(x + width), Math.ceil(Y));
			else
				line = new Line2D.Double(x, Y, x + width, Y);
			g.draw(line);
		}
		if (isLeftValid()) {
			g.setStroke(getLeftStroke());
			g.setColor(getLeftColor());

			X = x + getLeftStroke().getLineWidth() / 2.0;
			if (ceil)
				line = new Line2D.Double(Math.ceil(X), Math.floor(y), Math.ceil(X), Math.ceil(y + height));
			else
				line = new Line2D.Double(X, y, X, y + height);
			g.draw(line);
		}
		if (isBottomValid()) {
			g.setStroke(getBottomStroke());
			g.setColor(getBottomColor());

			Y = y + height - getBottomStroke().getLineWidth() / 2.0;
			if (ceil)
				line = new Line2D.Double(Math.ceil(x), Math.ceil(Y), Math.ceil(x + width), Math.ceil(Y));
			else
				line = new Line2D.Double(x, Y, x + width, Y);
			g.draw(line);
		}
		if (isRightValid()) {
			g.setStroke(getRightStroke());
			g.setColor(getRightColor());

			X = x + width - getRightStroke().getLineWidth() / 2.0;
			if (ceil)
				line = new Line2D.Double(Math.ceil(X), Math.ceil(y), Math.ceil(X), Math.ceil(y + height));
			else
				line = new Line2D.Double(X, y, X, y + height);
			g.draw(line);
		}

		g.setStroke(oldStroke);
		g.setColor(oldColor);
	}

	/**
	 * Returns the border insets.
	 * 
	 * @return
	 */
	public Insets getBorderInsets() {
		int top = 0;
		int right = 0;
		int bottom = 0;
		int left = 0;
		if (isTopValid())
			top = (int) getTopStroke().getLineWidth();
		if (isRightValid())
			right = (int) getRightStroke().getLineWidth();
		if (isBottomValid())
			bottom = (int) getBottomStroke().getLineWidth();
		if (isLeftValid())
			left = (int) getLeftStroke().getLineWidth();
		return new Insets(top, left, bottom, right);
	}

	/**
	 * @see javax.swing.border.Border#getBorderInsets(java.awt.Component)
	 */
	public Insets getBorderInsets(Component c) {
		int top = 0;
		int right = 0;
		int bottom = 0;
		int left = 0;
		if (isTopValid())
			top = (int) getTopStroke().getLineWidth();
		if (isRightValid())
			right = (int) getRightStroke().getLineWidth();
		if (isBottomValid())
			bottom = (int) getBottomStroke().getLineWidth();
		if (isLeftValid())
			left = (int) getLeftStroke().getLineWidth();
		return new Insets(top, left, bottom, right);
	}

	/**
	 * @see javax.swing.border.Border#isBorderOpaque()
	 */
	public boolean isBorderOpaque() {
		return false;
	}

	/**
	 * Are attributes valid to paint the top of the border.
	 * 
	 * @return
	 */
	public boolean isTopValid() {
		return topStroke != null && topColor != null;
	}

	/**
	 * Are attributes valid to paint the right of the border.
	 * 
	 * @return
	 */
	public boolean isRightValid() {
		return rightStroke != null && rightColor != null;
	}

	/**
	 * Are attributes valid to paint the bottom of the border.
	 * 
	 * @return
	 */
	public boolean isBottomValid() {
		return bottomStroke != null && bottomColor != null;
	}

	/**
	 * Are attributes valid to paint the left of the border.
	 * 
	 * @return
	 */
	public boolean isLeftValid() {
		return leftStroke != null && leftColor != null;
	}

	public Color getBottomColor() {
		return bottomColor;
	}

	public void setBottomColor(Color bottomColor) {
		this.bottomColor = bottomColor;
	}

	public BasicStroke getBottomStroke() {
		return bottomStroke;
	}

	public void setBottomStroke(BasicStroke bottomStroke) {
		this.bottomStroke = bottomStroke;
	}

	public Color getLeftColor() {
		return leftColor;
	}

	public void setLeftColor(Color leftColor) {
		this.leftColor = leftColor;
	}

	public BasicStroke getLeftStroke() {
		return leftStroke;
	}

	public void setLeftStroke(BasicStroke leftStroke) {
		this.leftStroke = leftStroke;
	}

	public Color getRightColor() {
		return rightColor;
	}

	public void setRightColor(Color rightColor) {
		this.rightColor = rightColor;
	}

	public BasicStroke getRightStroke() {
		return rightStroke;
	}

	public void setRightStroke(BasicStroke rightStroke) {
		this.rightStroke = rightStroke;
	}

	public Color getTopColor() {
		return topColor;
	}

	public void setTopColor(Color topColor) {
		this.topColor = topColor;
	}

	public BasicStroke getTopStroke() {
		return topStroke;
	}

	public void setTopStroke(BasicStroke topStroke) {
		this.topStroke = topStroke;
	}
}

package com.vasnabilisim.olive.core.renderer.export;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.LineRenderer;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class GLineRenderer extends GRenderer<LineRenderer> {

	@Override
	public Class<LineRenderer> rendererClass() {
		return LineRenderer.class;
	}
	
	@Override
	public void render(GraphicsExporter exporter, ReportRenderer reportRenderer, PageRenderer pageRenderer,
			LineRenderer r, Graphics2D g) throws BaseException {
		g.setColor(toColor(r.getColor()));
		g.setStroke(new BasicStroke(r.getThickness()));
		g.draw(new Line2D.Float(
				toPixels(exporter, reportRenderer.getPaper(), r.getFromPoint().x), 
				toPixels(exporter, reportRenderer.getPaper(), r.getFromPoint().y), 
				toPixels(exporter, reportRenderer.getPaper(), r.getToPoint().x), 
				toPixels(exporter, reportRenderer.getPaper(), r.getToPoint().y)
				)
		);
	}
}

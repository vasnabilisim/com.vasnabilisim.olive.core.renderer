package com.vasnabilisim.olive.core.renderer.export;

import java.awt.Graphics2D;
import java.io.OutputStream;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.paper.PaperUnit;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;
import com.vasnabilisim.olive.core.renderer.SimpleRenderer;

public abstract class GraphicsExporter implements Exporter {

	protected GraphicsExporter() {
	}

	@Override
	public void export(ReportRenderer reportRenderer, OutputStream out) throws BaseException {
		int pageIndex = 0;
		begin(out, reportRenderer);
		for(PageRenderer pageRenderer : reportRenderer.getPages()) {
			Graphics2D g = createGraphics(pageRenderer, pageIndex);
			for(SimpleRenderer simpleRenderer : pageRenderer.getChildren()) {
				GRenderer<SimpleRenderer> gRenderer = GRenderer.get(simpleRenderer);
				gRenderer.render(this, reportRenderer, pageRenderer, simpleRenderer, g);
			}
			++pageIndex;
		}
		end();
	}
	
	/**
	 * Called before exporting.
	 * @param out
	 * @param reportRenderer
	 * @throws BaseException
	 */
	protected abstract void begin(
			OutputStream out, 
			ReportRenderer reportRenderer
			) throws BaseException;
	
	/**
	 * Creates and returns graphics for given parameters. 
	 * Called for each page in report. 
	 * Same graphics may be used for all pages.
	 * @param pageRenderer
	 * @param pageIndex
	 * @return
	 * @throws BaseException
	 */
	protected abstract Graphics2D createGraphics(
			PageRenderer pageRenderer, 
			int pageIndex) throws BaseException;
	
	/**
	 * Called after exporting. 
	 * Handle closing or resource cleanig.
	 * @throws BaseException
	 */
	protected abstract void end() throws BaseException;

	/**
	 * Converts given length to pixels.
	 * @param unit
	 * @param value
	 * @return
	 */
	protected abstract float toPixels(PaperUnit unit, float value);
	
}

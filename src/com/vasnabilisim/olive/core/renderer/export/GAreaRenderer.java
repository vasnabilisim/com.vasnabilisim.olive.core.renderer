package com.vasnabilisim.olive.core.renderer.export;

import java.awt.Graphics2D;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.AreaRenderer;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

public abstract class GAreaRenderer<R extends AreaRenderer> extends GRenderer<R> {

	@Override
	public void render(GraphicsExporter exporter, ReportRenderer reportRenderer, PageRenderer pageRenderer, R r,
			Graphics2D g) throws BaseException {
		renderArea(exporter, reportRenderer, pageRenderer, r, g);
	}

	protected abstract void renderArea(GraphicsExporter exporter, ReportRenderer reportRenderer, PageRenderer pageRenderer, R r,
			Graphics2D g) throws BaseException;
}

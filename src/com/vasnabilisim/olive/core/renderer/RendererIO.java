package com.vasnabilisim.olive.core.renderer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.TreeMap;

import org.w3c.dom.Element;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.olive.core.property.Properties;
import com.vasnabilisim.olive.core.property.Property;
import com.vasnabilisim.olive.core.property.PropertyException;
import com.vasnabilisim.olive.core.property.PropertyGroup;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xml.XmlParser;

/**
 * @author Menderes Fatih GUVEN
 */
public final class RendererIO {

	private RendererIO() {
	}
	
	static TreeMap<String, Class<? extends SimpleRenderer>> rendererClasses = new TreeMap<>();
	
	static {
		registerRendererClass("Box", TextRenderer.class);
		registerRendererClass("Text", TextRenderer.class);
		registerRendererClass("Line", LineRenderer.class);
		registerRendererClass("Image", ImageRenderer.class);
	}

	public static <N extends SimpleRenderer> void registerRendererClass(String name, Class<N> rendererClass) {
		rendererClasses.put(name, rendererClass);
	}
	
	public static Class<? extends SimpleRenderer> getRendererClass(String name) {
		return rendererClasses.get(name);
	}
	
	public static SimpleRenderer createRenderer(String name) {
		Class<? extends SimpleRenderer> rendererClass = getRendererClass(name);
		if(rendererClass == null)
			return null;
		try {
			return rendererClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			Logger.log(e);
			return null;
		}
	}

	public static ReportRenderer read(String fileName) throws BaseException {
		return read(new File(fileName));
	}

	public static ReportRenderer read(File file) throws BaseException {
		try {
			return read(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new ErrorException(e);
		}
	}

	public static ReportRenderer read(InputStream in) throws BaseException {
		try {
			return read(new InputStreamReader(in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new ErrorException(e);
		}
	}

	public static ReportRenderer read(Reader reader) throws BaseException {
		org.w3c.dom.Document xmlDocument;
		try {
			xmlDocument = XmlParser.fromReaderToDocument(reader);
		} catch (XmlException e) {
			throw new ErrorException("Unable to parse document", e);
		}
		Element reportElement = xmlDocument.getDocumentElement();
		ReportRenderer reportRenderer = new ReportRenderer();
		readProperties(xmlDocument, reportElement, reportRenderer);
		List<Element> pageElements = XmlParser.findChildElements(reportElement, "Page");
		for(Element pageElement : pageElements) {
			PageRenderer pageRenderer = new PageRenderer();
			readChildren(xmlDocument, pageElement, pageRenderer);
			reportRenderer.addPage(pageRenderer);
		}
		return reportRenderer;
	}

	static void readChildren(org.w3c.dom.Document xmlDocument, Element containerElement, PageRenderer pageRenderer) {
		Element childrenElement = XmlParser.findChildElement(containerElement, "Children");
		if(childrenElement == null)
			return;
		List<Element> simpleRendererElements = XmlParser.allChildElements(childrenElement);
		for(Element simpleRendererElement : simpleRendererElements) {
			SimpleRenderer simpleRenderer = createRenderer(simpleRendererElement.getNodeName());
			if(simpleRenderer == null)
				continue;
			pageRenderer.addChild(simpleRenderer);
			readProperties(xmlDocument, simpleRendererElement, simpleRenderer);
		}
	}

	static void readProperties(org.w3c.dom.Document xmlDocument, Element element, Renderer renderer) {
		Properties properties = renderer.getProperties();
		for(PropertyGroup propertyGroup : properties.getGroups()) {
			for(Property<?> property : propertyGroup.getProperties()) {
				Element childElement = XmlParser.findChildElement(element, property.getName());
				if(childElement == null)
					continue;
				try {
					property.readXmlElement(xmlDocument, childElement);
				} catch (PropertyException e) {
					Logger.log(e);
				}
			}
		}
	}
}

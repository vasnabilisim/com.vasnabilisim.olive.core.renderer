package com.vasnabilisim.olive.core.renderer;

import java.io.Serializable;

import com.vasnabilisim.olive.core.property.Properties;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class Renderer implements Serializable, Cloneable {
	private static final long serialVersionUID = -2065867182898912096L;

	protected PageRenderer parent;
	protected Properties properties;
	
	protected Renderer() {
		this.properties = new Properties();
	}
	
	protected Renderer(Renderer source) {
		this.properties = new Properties();
	}
	
	public abstract String getId();
	
	@Override
	public abstract Renderer clone();
	
	public PageRenderer getParent() {
		return parent;
	}
	
	public void setParent(PageRenderer parent) {
		this.parent = parent;
	}
	
	public Properties getProperties() {
		return properties;
	}
}

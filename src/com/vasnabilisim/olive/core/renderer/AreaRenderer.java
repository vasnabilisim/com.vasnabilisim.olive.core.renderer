package com.vasnabilisim.olive.core.renderer;

import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.property.ColorProperty;
import com.vasnabilisim.olive.core.property.PropertyFactory;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AreaRenderer extends SimpleRenderer {
	private static final long serialVersionUID = -766253846045499601L;

	protected ColorProperty backgroundProperty;
	
	public AreaRenderer() {
		super();
		this.backgroundProperty = PropertyFactory.createColorProperty("Content", "Background", "Background", "Background color", Color.Transparent);
		this.properties.add(this.backgroundProperty);
	}
	
	public AreaRenderer(AreaRenderer source) {
		super(source);
		this.backgroundProperty = source.backgroundProperty.clone();
		this.properties.add(this.backgroundProperty);
	}
	
	public Color getBackground() {
		return backgroundProperty.getValue();
	}
	
	public void setBackground(Color value) {
		backgroundProperty.setValue(value);
	}
}

package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.olive.core.geom.Alignment;
import com.vasnabilisim.olive.core.geom.HorizontalAlignment;

/**
 * RTL Class.
 * Some useful right to left alignment methods.  
 * @author Menderes Fatih GUVEN 
 */
public final class RTL {

	private RTL() {
	}

	/**
	 * Returns final horizontal alignment from given parameters.
	 * @param rtl true: Right to left alignment, false: Left to right alignment.
	 * @param value Alignment to be evaluated.
	 * @return
	 */
	public static HorizontalAlignment get(boolean rtl, HorizontalAlignment value) {
		if(rtl) {
			switch (value) {
			case Leading:	return HorizontalAlignment.Right;
			case Trailing:	return HorizontalAlignment.Left;
			default:		return value;
			}
		}
		else {
			switch (value) {
			case Leading:	return HorizontalAlignment.Left;
			case Trailing:	return HorizontalAlignment.Right;
			default:		return value;
			}
		}
	}

	/**
	 * Returns final alignment from given parameters.
	 * @param rtl true: Right to left alignment, false: Left to right alignment.
	 * @param value Alignment to be evaluated.
	 * @return
	 */
	public static Alignment get(boolean rtl, Alignment alignment) {
		if(rtl) {
			switch (alignment) {
			case TopLeading:		return Alignment.TopRight;
			case CenterLeading:		return Alignment.CenterRight;
			case BottomLeading:		return Alignment.BottomRight;

			case TopTrailing:		return Alignment.TopLeft;
			case CenterTrailing:	return Alignment.CenterLeft;
			case BottomTrailing:	return Alignment.BottomLeft;
				
			default: 				return alignment;
			}
		}
		else {
			switch (alignment) {
			case TopLeading:		return Alignment.TopLeft;
			case CenterLeading:		return Alignment.CenterLeft;
			case BottomLeading:		return Alignment.BottomLeft;

			case TopTrailing:		return Alignment.TopRight;
			case CenterTrailing:	return Alignment.CenterRight;
			case BottomTrailing:	return Alignment.BottomRight;
				
			default: 				return alignment;
			}
		}
	}
}

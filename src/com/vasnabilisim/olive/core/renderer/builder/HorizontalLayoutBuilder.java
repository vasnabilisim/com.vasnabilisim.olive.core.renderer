package com.vasnabilisim.olive.core.renderer.builder;

import java.util.ArrayList;
import java.util.List;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.document.DesignNode;
import com.vasnabilisim.olive.core.document.HorizontalLayout;
import com.vasnabilisim.olive.core.document.HorizontalLayout.HorizontalLayoutConstraint;
import com.vasnabilisim.olive.core.geom.DimensionPolicy;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.renderer.BoxRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class HorizontalLayoutBuilder extends AbstractLayoutBuilder<HorizontalLayout> {

	public HorizontalLayoutBuilder() {
		super(HorizontalLayout.class);
	}
	
	//26.03.2015 MFG
	//Two iterations
	//1. Create renderers
	//2. Layout renderers

	@Override
	public BoxRenderer build(BuilderContext context, HorizontalLayout layout) throws BaseException {
		BoxRenderer boxRenderer = new BoxRenderer();

		//Area
		boxRenderer.setBackground(layout.getBackground());

		//Rectangular
		boxRenderer.setBorder(layout.getBorder());
		
		//TODO change bounds of the box at the end

		ArrayList<RendererContext> rendererContexts = new ArrayList<>();
		
		for(DesignNode child : layout.getChildren()) {
			RendererContext rc = new RendererContext(context, child);
			rendererContexts.add(rc);
			//containerRenderer.addChild(rc.renderer);
		}
		
		layout.getAlignment();
		
		switch (RTL.get(context.rtl, layout.getAlignment())) {
		case Left	: layoutLeft(context, layout, containerRenderer, rendererContexts); break;
		case Center	: layoutCenter(context, layout, containerRenderer, rendererContexts); break;
		case Right	: layoutRight(context, layout, containerRenderer, rendererContexts); break;
		default: break;
		}

		return boxRenderer;
	}
	
	/**
	 * A help class used to align sub renderers.
	 * @author Menderes Fatih GUVEN
	 */
	static class LayoutContext {
		/**
		 * Sum of all fill ratios of sub renderers.
		 */
		float fillRatioTotal;
		/**
		 * Length to be filled by sub renderers which has fill constrained.
		 */
		float fillWidthTotal;
		/**
		 * Height of the layout minus vertical border thicknesses and vertical padding thickness.
		 */
		float fillHeightTotal;
	}
	
	/**
	 * Calculates LayoutContext for given container renderer.
	 * @param containerRenderer
	 * @param rendererContexts
	 * @return
	 */
	protected LayoutContext calcLayoutContext(
			ContainerRenderer containerRenderer, 
			List<RendererContext> rendererContexts) 
	{
		LayoutContext lc = new LayoutContext();
		lc.fillRatioTotal = 0f;
		lc.fillWidthTotal = containerRenderer.getWidth();
		lc.fillHeightTotal = containerRenderer.getHeight();
		Insets border = containerRenderer.getBorder().thickness;
		lc.fillWidthTotal -= border.l;
		lc.fillHeightTotal -= border.t;
		Insets padding = containerRenderer.getPadding();
		lc.fillWidthTotal -= padding.l;
		lc.fillHeightTotal -= padding.t;
		for(RendererContext rc : rendererContexts) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) rc.layoutConstraint;
			Insets margin = constraint.getMargin();
			lc.fillWidthTotal -= margin.l;
			if(constraint.getWidthPolicy() == DimensionPolicy.Fixed)
				lc.fillWidthTotal -= rc.width;
			if(constraint.getWidthPolicy() == DimensionPolicy.Fill)
				lc.fillRatioTotal += constraint.getWidth();
			lc.fillWidthTotal -= margin.r;
		}
		lc.fillWidthTotal -= padding.r;
		lc.fillWidthTotal -= border.r;
		lc.fillHeightTotal -= padding.b;
		lc.fillHeightTotal -= border.b;
		
		lc.fillWidthTotal = lc.fillWidthTotal < 0f ? 0f : lc.fillWidthTotal;
		lc.fillHeightTotal = lc.fillHeightTotal < 0f ? 0f : lc.fillHeightTotal;
		lc.fillRatioTotal = lc.fillRatioTotal == 0f ? 1f : lc.fillRatioTotal;
		
		return lc;
	}
	
	/**
	 * Aligns content starting from left. 
	 * Each sub renderer aligned to right of previous one.  
	 * @param context
	 * @param layout
	 * @param containerRenderer
	 * @param rendererContexts
	 */
	protected void layoutLeft(
			BuilderContext context, 
			HorizontalLayout layout, 
			ContainerRenderer containerRenderer, 
			List<RendererContext> rendererContexts) 
	{
		LayoutContext lc = calcLayoutContext(containerRenderer, rendererContexts);
		Insets border = containerRenderer.getBorder().thickness;
		Insets padding = containerRenderer.getPadding();
		
		float x = border.l + padding.l;
		float y = border.t + padding.t;
		float w = 0f;
		float h = 0f;
		for(RendererContext rc : rendererContexts) {
			HorizontalLayoutConstraint constraint = (HorizontalLayoutConstraint) rc.layoutConstraint;
			Insets margin = constraint.getMargin();
			if(constraint.getWidthPolicy() == DimensionPolicy.Fixed) {
				w = rc.width;
				h = rc.height;
			}
			else if(constraint.getWidthPolicy() == DimensionPolicy.Fill) {
				//TODO Where is margin left and right
				w = lc.fillWidthTotal * constraint.getWidth() / lc.fillRatioTotal;
				//TODO Fill affects only width not height
				h = (lc.fillHeightTotal - margin.t - margin.b) * constraint.getHeight();
			}

			rc.renderer.setX(x + margin.l);
			switch (constraint.getAlignment()) {
			case Top	: rc.renderer.setY(y + margin.t); break;
			case Center	: rc.renderer.setY(y + margin.t + (lc.fillHeightTotal - h - margin.t - margin.b) / 2); break;
			case Bottom	: rc.renderer.setY(y + margin.t + (lc.fillHeightTotal - h - margin.t - margin.b)); break;
			}
			rc.renderer.setWidth(w);
			rc.renderer.setHeight(h);
			
			x += margin.l;
			x += w;
			x += margin.r;
		}
	}
	
	protected void layoutRight(
			BuilderContext context, 
			HorizontalLayout layout, 
			ContainerRenderer containerRenderer, 
			List<RendererContext> rendererContexts) 
	{
		LayoutContext lc = calcLayoutContext(containerRenderer, rendererContexts);
		Insets border = containerRenderer.getBorder().thickness;
		Insets padding = containerRenderer.getPadding();
	}
	
	protected void layoutCenter(
			BuilderContext context, 
			HorizontalLayout layout, 
			ContainerRenderer containerRenderer, 
			List<RendererContext> rendererContexts) 
	{
		LayoutContext lc = calcLayoutContext(containerRenderer, rendererContexts);
		Insets border = containerRenderer.getBorder().thickness;
		Insets padding = containerRenderer.getPadding();
	}
}

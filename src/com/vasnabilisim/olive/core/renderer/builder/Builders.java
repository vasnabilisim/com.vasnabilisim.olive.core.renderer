package com.vasnabilisim.olive.core.renderer.builder;

import java.util.TreeMap;

import com.vasnabilisim.olive.core.document.Node;
import com.vasnabilisim.olive.core.renderer.Renderer;
import com.vasnabilisim.util.ClassComparator;

/**
 * @author Menderes Fatih GUVEN
 */
public final class Builders {

	private Builders() {
	}

	private static TreeMap<Class<? extends Node>, Builder<?, ?>> BUILDER_MAP = new TreeMap<>(new ClassComparator());
	
	static {
		register(new StaticTextBuilder());
		register(new HorizontalLayoutBuilder());
		//TODO add more builders
	}
	
	public static 
	<N extends Node, R extends Renderer> void register(Builder<N, R> builder) {
		BUILDER_MAP.put(builder.nodeType(), builder);
	}
	
	@SuppressWarnings("unchecked")
	public static <N extends Node, R extends Renderer> Builder<N, R> get(Class<N> nodeType) {
		return (Builder<N, R>) BUILDER_MAP.get(nodeType);
	}

	public static Builder<?, ?> getInternal(Class<? extends Node> nodeType) {
		return BUILDER_MAP.get(nodeType);
	}
}

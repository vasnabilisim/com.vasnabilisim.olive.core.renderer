package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.document.Node;
import com.vasnabilisim.olive.core.renderer.Renderer;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Builder<N extends Node, R extends Renderer> {

	/**
	 * Returns the node type that is used to build renderer.
	 * @return
	 */
	Class<N> nodeType();

	/**
	 * Returns the renderer type that is build by this builder.
	 * @return
	 */
	Class<R> rendererType();

	/**
	 * Uses given node on given context and returns the build renderer.
	 * @param context
	 * @param node
	 * @return
	 * @throws BaseException
	 */
	R build(BuilderContext context, N node) throws BaseException;
}

package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.olive.core.document.Node;
import com.vasnabilisim.olive.core.renderer.Renderer;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractBuilder<N extends Node, R extends Renderer> implements Builder<N, R> {

	Class<N> nodeType;
	Class<R> rendererType;

	/**
	 * Type constructor. 
	 * @see Builder#nodeType()
	 * @see Builder#rendererType()
	 * @param nodeType
	 * @param rendererType
	 */
	protected AbstractBuilder(Class<N> nodeType, Class<R> rendererType) {
		this.nodeType = nodeType;
		this.rendererType = rendererType;
	}

	/**
	 * @see com.vasnabilisim.olive.core.renderer.builder.Builder#nodeType()
	 */
	@Override
	public Class<N> nodeType() {
		return nodeType;
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.renderer.builder.Builder#rendererType()
	 */
	@Override
	public Class<R> rendererType() {
		return rendererType;
	}
	
	/**
	 * Sets renderer property with the value from node.
	 * @param node
	 * @param renderer
	 * @param groupName
	 * @param propertyName
	 */
	protected static void transferProperty(Node node, Renderer renderer, String groupName, String propertyName) {
		transferProperty(node, groupName, propertyName, renderer, groupName, propertyName);
	}
	
	/**
	 * Sets renderer property with the value from node.
	 * @param node
	 * @param nodeGroupName
	 * @param nodePropertyName
	 * @param renderer
	 * @param rendererGroupName
	 * @param rendererPropertyName
	 */
	protected static void transferProperty(
			Node node, String nodeGroupName, String nodePropertyName, 
			Renderer renderer, String rendererGroupName, String rendererPropertyName) 
	{
		renderer.getProperties().setValue(
				rendererGroupName, 
				rendererPropertyName, 
				node.getProperties().getValue(nodeGroupName, nodePropertyName)
		);
	}
}

package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.olive.core.document.StaticImage;
import com.vasnabilisim.olive.core.renderer.ImageRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class ImageBuilder extends AbstractBuilder<StaticImage, ImageRenderer> {

	public ImageBuilder() {
		super(StaticImage.class, ImageRenderer.class);
	}

	@Override
	public ImageRenderer build(BuilderContext context, StaticImage node) {
		ImageRenderer renderer = new ImageRenderer();

		//Area
		renderer.setBackground(node.getBackground());
		
		//Rectangular
		renderer.setBorder(node.getBorder());

		//Image
		renderer.setImage(node.getImage());
		renderer.setPaintPolicy(node.getPaintPolicy());
		renderer.setAlignment(RTL.get(context.isRtl(), node.getAlignment()));

		return renderer;
	}
}

package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.olive.core.document.Document;
import com.vasnabilisim.olive.core.document.Part;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class BuilderContext {

	boolean rtl;
	Obj rootParameterObject;
	Obj rootVariableObject;
	Paper page;
	Document document;
	ReportRenderer reportRenderer;
	Part part;
	PageRenderer pageRenderer;
	
	float y;
	
	public BuilderContext(
			boolean rtl, 
			Obj rootParameterObject, 
			Obj rootVariableObject, 
			Paper page, 
			Document document, 
			ReportRenderer reportRenderer) 
	{	
		this.rtl = rtl;
		this.rootParameterObject = rootParameterObject;
		this.rootVariableObject = rootVariableObject;
		this.page = page;
		this.document = document;
		this.reportRenderer = reportRenderer;
	}

	public boolean isRtl() {
		return rtl;
	}
	
	public Obj getParameterObject() {
		return rootParameterObject;
	}
	
	public Obj getVariableObject() {
		return rootVariableObject;
	}
	
	public Paper getPage() {
		return page;
	}
	
	public Document getDocument() {
		return document;
	}
	
	public ReportRenderer getReportRenderer() {
		return reportRenderer;
	}
	
	public Part getPart() {
		return part;
	}
	
	public void setPart(Part part) {
		this.part = part;
	}
	
	public PageRenderer getPageRenderer() {
		return pageRenderer;
	}
	
	public void setPageRenderer(PageRenderer pageRenderer) {
		this.pageRenderer = pageRenderer;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
}

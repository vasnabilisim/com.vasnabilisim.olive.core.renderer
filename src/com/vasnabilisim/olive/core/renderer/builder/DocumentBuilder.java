package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.WarningException;
import com.vasnabilisim.olive.core.document.Document;
import com.vasnabilisim.olive.core.document.Part;
import com.vasnabilisim.olive.core.document.PartType;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class DocumentBuilder extends AbstractBuilder<Document, ReportRenderer> {

	public DocumentBuilder() {
		super(Document.class, ReportRenderer.class);
	}
	
	public ReportRenderer build(
			boolean rtl, 
			Obj rootParameterObject, 
			Obj rootVariableObject, 
			Document document) throws BaseException 
	{
		ReportRenderer reportRenderer = new ReportRenderer();
		reportRenderer.setPaper(document.getPaper());
		BuilderContext context = new BuilderContext(
				rtl, 
				rootParameterObject, 
				rootVariableObject, 
				document.getPaper(), 
				document, 
				reportRenderer);
		build(context, document);
		return reportRenderer;
	}

	@Override
	public ReportRenderer build(BuilderContext context, Document node) throws BaseException {
		PageRenderer pageRenderer = new PageRenderer();
		context.getReportRenderer().addPage(pageRenderer);
		buildReportHeader(context);
		return context.getReportRenderer();
	}
	
	void buildReportHeader(BuilderContext context) throws BaseException {
		float height = 0f;
		for(Part part : context.getDocument().getParts()) {
			if(part.getTypes().contains(PartType.ReportHeader))
				height += part.getHeight();
		}
		if(height > context.getPage().getOrientedImageableHeight())
			throw new WarningException(
					String.format("Report headers total height must be less then page imageable height. "
							+ "Page height: %f, Report header height: %f.", 
							context.getPage().getOrientedImageableHeight(), 
							height));
		for(Part part : context.getDocument().getParts()) {
			if(part.getTypes().contains(PartType.ReportHeader)) {
				context.setPart(part);
				buildPart(context);
			}
		}
	}
	
	void buildPart(BuilderContext context) {
		//TODO implement
	}
}

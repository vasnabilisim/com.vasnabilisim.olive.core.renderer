package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.document.DesignNode;
import com.vasnabilisim.olive.core.document.Layout;
import com.vasnabilisim.olive.core.document.LayoutConstraint;
import com.vasnabilisim.olive.core.renderer.BoxRenderer;
import com.vasnabilisim.olive.core.renderer.Renderer;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractLayoutBuilder<L extends Layout> extends AbstractBuilder<L, BoxRenderer> {

	public AbstractLayoutBuilder(Class<L> nodeType) {
		super(nodeType, BoxRenderer.class);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected static Renderer build(BuilderContext context, DesignNode node) throws BaseException {
		Builder builder = Builders.getInternal(node.getClass());
		return builder.build(context, node);
	}
	
	protected static class RendererContext {
		public DesignNode node;
		public Renderer renderer;
		public LayoutConstraint layoutConstraint;
		
		public RendererContext(BuilderContext context, DesignNode node) throws BaseException {
			this.node = node;
			this.renderer = build(context, node);
			this.layoutConstraint = node.getLayoutConstraint();
		}
	}
}

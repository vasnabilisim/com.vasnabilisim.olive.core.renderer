package com.vasnabilisim.olive.core.renderer.builder;

import com.vasnabilisim.olive.core.document.StaticText;
import com.vasnabilisim.olive.core.renderer.TextRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class StaticTextBuilder extends AbstractBuilder<StaticText, TextRenderer> {

	public StaticTextBuilder() {
		super(StaticText.class, TextRenderer.class);
	}

	@Override
	public TextRenderer build(BuilderContext context, StaticText node) {
		TextRenderer renderer = new TextRenderer();

		//Area
		renderer.setBackground(node.getBackground());
		
		//Rectangular
		renderer.setBorder(node.getBorder());

		//Text
		renderer.setForeground(node.getForeground());
		renderer.setText(node.getText());
		renderer.setAlignment(RTL.get(context.isRtl(), node.getAlignment()));
		renderer.setFont(node.getFont());
		renderer.setMaximumLines(node.getMaximumLines());

		return renderer;
	}
}

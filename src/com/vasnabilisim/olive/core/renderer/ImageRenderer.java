package com.vasnabilisim.olive.core.renderer;

import java.awt.image.BufferedImage;

import com.vasnabilisim.olive.core.geom.Alignment;
import com.vasnabilisim.olive.core.paint.ImagePaintPolicy;
import com.vasnabilisim.olive.core.property.AlignmentProperty;
import com.vasnabilisim.olive.core.property.ImagePaintPolicyProperty;
import com.vasnabilisim.olive.core.property.ImageProperty;
import com.vasnabilisim.olive.core.property.PropertyFactory;

/**
 * @author Menderes Fatih GUVEN
 */
public class ImageRenderer extends RectangularRenderer {
	private static final long serialVersionUID = -1959614278979206025L;

	protected ImageProperty imageProperty;
	protected ImagePaintPolicyProperty paintPolicyProperty;
	protected AlignmentProperty alignmentProperty;
	
	public ImageRenderer() {
		super();
		this.imageProperty = PropertyFactory.createImageProperty("Content", "Image", "Image", "Image to paint", null);
		this.paintPolicyProperty = PropertyFactory.createImagePaintPolicyProperty("Content", "Paint Policy", "PaintPolicy", "Image paint policy", ImagePaintPolicy.Single);
		this.alignmentProperty = PropertyFactory.createAlignmentProperty("Content", "Alignment", "Alignment", "Alignment of content", Alignment.TopLeft);
		this.properties.add(this.imageProperty);
		this.properties.add(this.paintPolicyProperty);
		this.properties.add(this.alignmentProperty);
	}
	
	public ImageRenderer(ImageRenderer source) {
		super(source);
		this.imageProperty = source.imageProperty.clone();
		this.paintPolicyProperty = source.paintPolicyProperty.clone();
		this.alignmentProperty = source.alignmentProperty.clone();
		this.properties.add(this.imageProperty);
		this.properties.add(this.paintPolicyProperty);
		this.properties.add(this.alignmentProperty);
	}
	
	@Override
	public String getId() {
		return "Image";
	}

	@Override
	public ImageRenderer clone() {
		return new ImageRenderer(this);
	}
	
	public BufferedImage getImage() {
		return imageProperty.getValue();
	}
	
	public void setImage(BufferedImage value) {
		imageProperty.setValue(value);
	}
	
	public ImagePaintPolicy getPaintPolicy() {
		return paintPolicyProperty.getValue();
	}
	
	public void setPaintPolicy(ImagePaintPolicy value) {
		paintPolicyProperty.setValue(value);
	}
	
	public Alignment getAlignment() {
		return alignmentProperty.getValue();
	}
	
	public void setAlignment(Alignment value) {
		alignmentProperty.setValue(value);
	}
}

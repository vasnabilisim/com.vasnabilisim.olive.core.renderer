package com.vasnabilisim.olive.core.renderer;

import com.vasnabilisim.olive.core.geom.Alignment;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.property.AlignmentProperty;
import com.vasnabilisim.olive.core.property.ColorProperty;
import com.vasnabilisim.olive.core.property.FontProperty;
import com.vasnabilisim.olive.core.property.InsetsProperty;
import com.vasnabilisim.olive.core.property.IntegerProperty;
import com.vasnabilisim.olive.core.property.PropertyFactory;
import com.vasnabilisim.olive.core.property.StringProperty;

/**
 * @author Menderes Fatih GUVEN
 */
public class TextRenderer extends RectangularRenderer {
	private static final long serialVersionUID = -5721272376036286485L;

	protected InsetsProperty paddingProperty;
	protected ColorProperty foregroundProperty;
	protected StringProperty textProperty;
	protected AlignmentProperty alignmentProperty;
	protected FontProperty fontProperty;
	protected IntegerProperty maximumLinesProperty;

	public TextRenderer() {
		super();
		this.paddingProperty = PropertyFactory.createInsetsProperty("Content", "Padding", "Padding", "Padding left, top, right, bottom", Insets.Zero.clone());
		this.foregroundProperty = PropertyFactory.createColorProperty("Text", "Color", "Foreground", "Text color", Color.Black);
		this.textProperty = PropertyFactory.createStringProperty("Text", "Text", "Text", "Text to write", null);
		this.alignmentProperty = PropertyFactory.createAlignmentProperty("Text", "Alignment", "Alignment", "Alignment of content", Alignment.CenterLeading);
		this.fontProperty = PropertyFactory.createFontProperty("Text", "Font", "Font", "Font of the text", Font.Default);
		this.maximumLinesProperty = PropertyFactory.createIntegerProperty("Text", "Maximum Lines", "MaximumLines", "Maximum number of lines", 1);
		this.properties.add(this.paddingProperty);
		this.properties.add(this.foregroundProperty);
		this.properties.add(this.textProperty);
		this.properties.add(this.alignmentProperty);
		this.properties.add(this.fontProperty);
		this.properties.add(this.maximumLinesProperty);
	}
	
	public TextRenderer(TextRenderer source) {
		super(source);
		this.paddingProperty = source.paddingProperty.clone();
		this.foregroundProperty = source.foregroundProperty.clone();
		this.textProperty = source.textProperty.clone();
		this.alignmentProperty = source.alignmentProperty.clone();
		this.fontProperty = source.fontProperty.clone();
		this.maximumLinesProperty = source.maximumLinesProperty.clone();
		this.properties.add(this.foregroundProperty);
		this.properties.add(this.textProperty);
		this.properties.add(this.alignmentProperty);
		this.properties.add(this.fontProperty);
		this.properties.add(this.maximumLinesProperty);
		this.properties.add(this.paddingProperty);
	}
	
	@Override
	public String getId() {
		return "Text";
	}

	@Override
	public TextRenderer clone() {
		return new TextRenderer(this);
	}

	public Color getForeground() {
		return foregroundProperty.getValue();
	}
	
	public void setForeground(Color value) {
		foregroundProperty.setValue(value);
	}
	
	public String getText() {
		return textProperty.getValue();
	}
	
	public void setText(String value) {
		textProperty.setValue(value);
	}
	
	public Alignment getAlignment() {
		return alignmentProperty.getValue();
	}
	
	public void setAlignment(Alignment value) {
		alignmentProperty.setValue(value);
	}
	
	public Font getFont() {
		return fontProperty.getValue();
	}
	
	public void setFont(Font value) {
		fontProperty.setValue(value);
	}
	
	public Integer getMaximumLines() {
		return maximumLinesProperty.getValue();
	}
	
	public void setMaximumLines(Integer value) {
		maximumLinesProperty.setValue(value);
	}
	
	public Insets getPadding() {
		return paddingProperty.getValue();
	}
	
	public void setPadding(Insets value) {
		paddingProperty.setValue(value);
	}
}
